<?php


namespace Tuandz\Hello\Model;


class Post extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface
{
    const CACHE_TAG = 'tuandz_hello_post';

    protected $_cacheTag = 'tuandz_hello_post';

    protected $_eventPrefix = 'tuandz_hello_post';

    protected function _construct()
    {
        $this->_init('Tuandz\Hello\Model\ResourceModel\Post');
    }

        public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

        public function getDefaultValues()
    {
        $values = [];

        return $values;
    }
}
