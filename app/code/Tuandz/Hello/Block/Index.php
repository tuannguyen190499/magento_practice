<?php

namespace Tuandz\Hello\Block;

class Index extends \Magento\Framework\View\Element\Template
{
    protected $postFactory;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Tuandz\Hello\Model\PostFactory $postFactory
    )
    {
        $this->postFactory = $postFactory;
        parent::__construct($context);
    }

    public function getPostCollection()
    {
        $post = $this->postFactory->create();
        $collection = $post->getCollection();
        return $collection;
    }
}
